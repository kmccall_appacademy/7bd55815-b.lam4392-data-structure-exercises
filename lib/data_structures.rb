# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  sorted = arr.sort
  sorted.last - sorted.first
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = ['a', 'e', 'i', 'o', 'u']
  string_vowels = []
  str.each_char do |char|
    string_vowels << char if vowels.include?(char.downcase)
  end
  string_vowels.length
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  str.delete('aeiouAEIOU')
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  digits_array = int.to_s.split('')
  digits_array.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  str.downcase.split('') != str.downcase.split('').uniq
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  area_code = arr[0..2].join
  first_three_digits = arr[3..5].join
  last_four_digits = arr[6..-1].join
  "(#{area_code}) #{first_three_digits}-#{last_four_digits}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  string_array = str.split(',').sort
  string_array.last.to_i - string_array.first.to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  if offset < 0
    offset_abs = arr.length + offset
  elsif offset > arr.length
    offset_abs = arr.length - 1
  else
    offset_abs = offset
  end
  arr.drop(offset_abs) + arr.take(offset_abs)
end
